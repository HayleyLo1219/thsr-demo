const stopSelect = {
	template:`
	<select @change="changeStop" v-model="selectedStop" class="form-control">
        <option value="default">chose</option>
        <option v-for="(stop, index) in stops" :inde="index" :value="stop.id">{{stop.name.tw}} {{stop.name.en}}</option>
    </select>
	`,
	props: ["stops", "direction"],
	data: function(){
		return {
			dataisload: false,
			selectedStop: 'default'
		}
	},
	created: function(){
	},
	methods: {
		changeStop: function(){
			this.$emit("changestop", { direction: this.direction, stop: this.selectedStop});
		}
	}
};

module.exports = stopSelect;