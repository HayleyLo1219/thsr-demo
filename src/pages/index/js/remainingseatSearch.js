import stopSelect from '@page/common/js/modules/stopSelect';

const remainingseatSearch = {
	template: `
	<div id="tab-content-1">
        <div class="row">
            <div class="col-md-4 col-12">
                <div class="user-input-area mb-5">
                    <div class="row">
                        <div class="col-12 mb-4">
                            <div class="mb-2 text-secondary">FROM</div>
                            <div>
                                <stop-select :stops="stops" :direction="'origin'" v-on:changestop="getSelectedStop"></stop-select>
                            </div>
                        </div>
                        <div class="col-12 mb-4">
                            <div><a @click.prevent="getTimetable" href="#" :class="{'disabled': searching}" class="btn btn-primary d-block">SEARCH</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-12">
                <div class="h5 mb-3 text-secondary">{{nowdate}} 時刻表</div>
                <div class="time-card">
                    <div v-if="searching">searching...</div>
                    <ul class="list-group w-md-50">
                        <li v-for="(table, index) in nowTimeTable" class="list-group-item p-0">
                                    <div class="p-3 mb-3 bg-light">
                                        <div class="thsr-no text-primary m-0 pb-2 h5">#車次 {{table.TrainNo}}</div>
                                        <div class="thsr-time m-0"><span class="h3">{{table.DepartureTime}}</span> <span class="h4">發車</span></div>
                                    </div>
                                    <div v-for="(stop, index) in table.StopStations" class="p-3 border-bottom">
                                        <div class=" mb-2 text-secondary h5">to {{stop.StationName.Zh_tw + ' ' + stop.StationName.En}}</div>
                                        <div class="seat" :class="{'text-success': stop.StandardSeatStatus === 'Available', 'text-danger': stop.StandardSeatStatus === 'Full', 'text-warning':stop.StandardSeatStatus === 'Limited'}">StandardSeat {{stop.StandardSeatStatus}}</div>
                                        <div class="seat" :class="{'text-success': stop.BusinessSeatStatus === 'Available', 'text-danger': stop.BusinessSeatStatus === 'Full', 'text-warning':stop.BusinessSeatStatus === 'Limited'}">BusinessSeat {{stop.BusinessSeatStatus}}</div>
                                    </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
	`,
    components: {stopSelect},
    props: ["stops", "nowdate"],
	data: function(){
		return {
			origin: 'default',
            destination: 'default',
			originData: [],
            nowTimeTable: [],
            timetableIsLoaded: false,
            searching: false
		}
	},
	methods:{
		getTimetable: function(){
            const _this = this;
            if (this.origin === 'default') {
                alert("請選擇欲查詢車站");
                return;
            }
            _this.originData=[];
            this.nowTimeTable = [];
            _this.searching = true;
            $.ajax({
                type: 'GET',
                url: `https://ptx.transportdata.tw/MOTC/v2/Rail/THSR/AvailableSeatStatusList/${_this.origin}?$top=9999&$orderby=DepartureTime`,
                dataType: 'json',
                // headers: getAuthorizationHeader(),
                success: function (res) {
                    _this.originData = res.AvailableSeats;
                    _this.filterStation();
                }
            });

		},
        getNowTime: function(){
            const date = new Date();
            return date.getHours() + ':' + date.getMinutes()
        },
        filterStation: function(){
            const _this = this;
            this.originData.forEach(function(el, idx){
                if (el.DepartureTime > _this.getNowTime()) {
                    _this.nowTimeTable.push(el)
                }
            })

           this.searching = false;
        },
        getSelectedStop: function(val){
            val.direction === 'origin' ? this.origin = val.stop: this.destination = val.stop;
        }
	},
	created: function(){
    }
}

module.exports = remainingseatSearch;