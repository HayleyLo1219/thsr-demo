import stopSelect from '@page/common/js/modules/stopSelect';

const timetableSearch = {
	template: `
	<div id="tab-content-1">
        <div class="row">
            <div class="col-md-4 col-12">
                <div class="user-input-area mb-5">
                    <div class="row">
                        <div class="col-12 mb-4">
                            <div class="mb-2 text-secondary">FROM</div>
                            <div>
                                <stop-select :stops="stops" :direction="'origin'" v-on:changestop="getSelectedStop"></stop-select>
                            </div>
                        </div>
                        <div class="col-12 mb-4">
                            <div class="mb-2 text-secondary">TO</div>
                            <div>
                                <stop-select :stops="stops" :direction="'destination'" v-on:changestop="getSelectedStop"></stop-select>
                            </div>
                        </div>
                        <div class="col-12 mb-4">
                            <div><a @click.prevent="getTimetable" href="#" :class="{'disabled': searching}" class="btn btn-primary d-block">SEARCH</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-12">
                <div class="h5 mb-3 text-secondary">{{nowdate}} 時刻表</div>
                <div class="time-card">
                    <div v-if="searching">searching...</div>
                    <ul class="list-group w-md-50">
                        <li v-for="(result, index) in timetableResult" :index="index" class="list-group-item">
                            <div class="thsr-no text-primary mb-2 h5">#車次 {{result.trainNo}}</div>
                            <div class="thsr-time h4">{{result.originDepartureTime}} - {{result.destinationArrivalTime}}</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
	`,
    components: {stopSelect},
    props: ["stops", "nowdate"],
	data: function(){
		return {
			origin: 'default',
			destination: 'default',
			timetableResult: [],
            timetableIsLoaded: false,
            searching: false
		}
	},
	methods:{
		getTimetable: function(){
            const _this = this;
            if (this.origin === 'default' || this.destination === 'default') {
                alert("請選擇起迄站");
                return;
            }
            if (this.origin === this.destination) {
                alert("起迄站不能相同，請重新選擇");
                return;
            }
            _this.timetableResult=[];
            _this.searching = true;
            $.ajax({
                type: 'GET',
                url: `https://ptx.transportdata.tw/MOTC/v2/Rail/THSR/DailyTimetable/OD/${_this.origin}/to/${_this.destination}/${_this.nowdate}?$orderby=OriginStopTime/DepartureTime&$top=9999`,
                dataType: 'json',
                // headers: getAuthorizationHeader(),
                success: function (res) {
                   for (var i = 0; i < res.length; i++) {
                        _this.timetableResult.push({
                            trainNo: res[i].DailyTrainInfo.TrainNo, 
                            originDepartureTime: res[i].OriginStopTime.DepartureTime, 
                            destinationArrivalTime: res[i].DestinationStopTime.ArrivalTime
                        });
                    };
                    _this.searching = false;
                }
            });

		},
        getSelectedStop: function(val){
            val.direction === 'origin' ? this.origin = val.stop: this.destination = val.stop;
        }
	},
	created: function(){
    }
}

module.exports = timetableSearch;