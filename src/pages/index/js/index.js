import common from '@page/common/js/common';
import timetableSearch from '@page/index/js/timetableSearch';
import thrsstopSearch from '@page/index/js/thrsstopSearch';
import remainingseatSearch from '@page/index/js/remainingseatSearch';

new Vue({
	el: '#app-thsr',
	template: `<div>
		<div class="d-md-block d-none">
			<ul class="menu-tabs h4 text-center">
				<li class="mr-4" v-for="(menu, index) in menus" :index="index">
					<a @click.prevent="clickTab(index)" :class="{'active': selectedMenu === index}" href="#">{{menu.name}}</a>
				</li>
			</ul>
		</div>
		<div class="d-block d-md-none">
			<select v-model="selectedMenu" class="form-control-lg bg-light w-100">
				<option v-for="(menu, index) in menus" :index="index" :value="index">{{menu.name}}</option>
			</select>
		</div>
		<div class="data-content">
			<timetable-search v-if="selectedMenu === 0 && stopisloaded" :stops="allStops" :nowdate="nowDate"></timetable-search>
			<thrsstop-search v-if="selectedMenu === 1"></thrsstop-search>
			<remainingseat-search v-if="selectedMenu === 2 && stopisloaded" :stops="allStops" :nowdate="nowDate"></remainingseat-search>
		</div>
	</div>`,
	components: {timetableSearch,thrsstopSearch,remainingseatSearch},
	data: {
		allStops: [],
		selectedMenu: 0,
		stopisloaded: false,
		nowDate: null,
		menus: [
			{name: '時刻表查詢'},
			{name: '停靠站查詢'},
			{name: '剩餘車位查詢'}
		]

	},
	created: function(){
		this.getInitStop();
		this.getDate();
	},
	methods: {
		getDate: function(){
			let date = new Date();
			let y = date.getFullYear();
			let m = date.getMonth()+1;
			let d = date.getDate();
			this.nowDate = y+'-' + (m < 10 ? '0' + m : m) + '-'+ (d<10 ? '0'+d:d);
		},
		getInitStop: function(){
			const _this = this;
			$.ajax({
			    type: 'GET',
			    url: 'https://ptx.transportdata.tw/MOTC/v2/Rail/THSR/Station?$format=JSON',
			    dataType: 'json',
			    // headers: getAuthorizationHeader(),
			    success: function (res) {
			    	_this.stopisloaded = true;
			        for (var i = 0; i < res.length; i++) {
			        	_this.allStops.push({
			        		uid: res[i].StationUID, 
			        		id: res[i].StationID, 
			        		name: {tw: res[i].StationName.Zh_tw, en: res[i].StationName.En}
			        	});
			        };
			    }
			});
		},
		clickTab: function(idx){
			this.selectedMenu = idx;
		}
	}
})
