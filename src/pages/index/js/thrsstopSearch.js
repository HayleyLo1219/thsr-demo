const thrsstopSearch = {
	template: `
	<div id="tab-content-2">
        <div class="row">
            <div class="col-md-4 col-12">
                <div class="user-input-area mb-5">
                    <div class="row">
                        <div class="col-12 mb-4">
                            <div class="mb-2 text-secondary">TRAIN NO.</div>
                            <div>
                                <input class="form-control" v-model="trainno" placeholder="please enter train No.">
                            </div>
                        </div>
                        <div class="col-12 mb-4">
                            <div><a @click.prevent="getStops" href="#" :class="{'disabled': searching}" class="btn btn-primary d-block">SEARCH</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-12">
                <div id="tab-content-2">
                <div v-if="searching">searching...</div>
                <ol v-if="stopResult.length!==0" class="progress-approval">
                    <li v-for="(result, index) in stopResult" :index="index" class="progress-step">
                        <div class="approver"><span class="name-tw mr-2 h5">{{result.name.tw}}</span><span class="name-en text-secondary h6">{{result.name.en}}</span></div>
                    </li>
                </ol>
            </div>
            </div>
        </div>
    </div>
	`,
	data: function(){
		return {
			trainno: null,
			stopResult: [],
            searching: false
		}
	},
	methods:{
		getStops: function(){
            const _this = this;
            if (this.trainno.length===0) {
                alert("請輸入車次代碼");
                return;
            }
            if (this.trainno.length!==4 || isNaN(this.trainno)) {
                alert("車次格式有誤，請重新輸入");
                return;
            }
            _this.stopResult=[];
            _this.searching = true;
            $.ajax({
                type: 'GET',
                url: `https://ptx.transportdata.tw/MOTC/v2/Rail/THSR/DailyTimetable/Today/TrainNo/${_this.trainno}`,
                dataType: 'json',
                // headers: getAuthorizationHeader(),
                success: function (res) {
                    if (res.length === 0) {
                        alert("查無結果");
                    } else {
                        const arr = res[0].StopTimes;
                       for (var i = 0; i < arr.length; i++) {
                            _this.stopResult.push({
                                stationID: arr[i].StationID,
                                name: {tw: arr[i].StationName.Zh_tw, en: arr[i].StationName.En}
                            });
                        };
                    }
                   
                    _this.searching = false;
                }
            });

		}
	},
	created: function(){
    }
}

module.exports = thrsstopSearch;