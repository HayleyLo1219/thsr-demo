# thsr-demo-page (using pug, vuejs, scss/sass, JQ)

## 使用

``` shell
git clone 
cd [__dirname]
npm install
npm run build:vendors (先打包第三方套件 vuejs/jq)
npm run dev (本機端run程式)
npm run build (打包檔案)
```


## 專案結構 (多頁面架構)

```
.
├── README.md
├── build
│   ├── build.js
│   ├── dev-client.js
│   ├── dev.js
│   ├── util.js
│   ├── webpack.base.config.js
│   ├── webpack.dev.config.js
│   ├── webpack.dll.config.js
│   └── webpack.prod.config.js
├── config
│   ├── base.js
│   ├── dev.env.js
│   ├── index.js
│   ├── prod.env.js
│   └── util.js
├── package-lock.json
├── package.json
└── src
    ├── pages
    │   ├── common (全站共用)
    │   │   └── js
    │   │       └── common.js
    │   ├── index (頁面結構)
    │      ├── index.html
    │      ├── js
    │           ├── index.js
    │     
    │   
    │       
    └── static (靜態資源文件)
        ├── libs
              └── js
                   ├── manifest_vendors.json
                   └── vendors.js
        
```

### 每個頁面的基本結構

```
 page-name (頁面名稱)
├── index.pug
├── js
     └── index.js (入口)
```